## Global Variables
rooms = ["courtyard", "church", "kitchen", "ruins", "basement"]
fileref = open("Castiglione.txt", "r")
room_descriptions = fileref.read().split("---")

## Main function
def app():
    start = input("Welcome to the castle! Would you like instructions? (y/n) - ")
    if start == "y":
        print("w = move forward/north\na = move left/west\ns = move back/south\nd = move right/east\nlocation = give name of current room")
    user_input(0, 0)

def user_input(current_room, keys):
    movement = input("What do you want to do? - ")
    state(movement, current_room, keys)

def state(movement, current_room, keys):
    if current_room == 0:
        if movement == "w":
            pickup = input(room_descriptions[1])
            if pickup == "y":
                print("You pick up the key and put it in your pocket.\n")
                user_input(1, keys+1)
            elif pickup == "n":
                print("You leave the key and continue looking around the church.\n")
                user_input(1, keys)
            user_input(1, keys)
        elif movement == "a":
            print(room_descriptions[2])
            user_input(2, keys)
        elif movement == "d":
            pickup = input(room_descriptions[3])
            if pickup == "y":
                print("You pick up the key and put it in your pocket.\n")
                user_input(3, keys+1)
                print(keys)
            elif pickup == "n":
                print("You leave the key and continue looking around the ruins.\n")
                user_input(3, keys)
            user_input(3, keys)
        elif movement == "s" and keys == 3:
            print("You take a look a the gate put together the fragments of the key you have collected.\n They fit perfectly into one piece, which you stick in the keyhole and twist.\n To your surprise, the gate creaks open, and you leave the courtyard without looking back.\n The end!")
            quit()
        elif movement == "s":
            print("You see a gate, but it's locked.\n")
            user_input(0, keys)
        else:
            print("You can't go that way.\n")
            user_input(0, keys)
    elif current_room == 1:
        if movement == "s":
            print("You leave the church and go back to the courtyard.\n")
            user_input(0, keys)
        else:
            print("You can't go that way.\n")
            user_input(1, keys)
    elif current_room == 2:
        if movement == "d":
            print("You leave the kitchen and go back to the courtyard.\n")
            user_input(0, keys)
        elif movement == "a":
            print("You open the door to the cellar.\n")
            user_input(4, keys)
        else:
            print("You can't go that way.\n")
            user_input(2, keys)
    elif current_room == 3:
        if movement == "a":
            print("You leave the ruins and go back to the courtyard.\n")
            user_input(0, keys)
        elif movement == "d":
            print(room_descriptions[5])
            user_input(5, keys)
        else:
            print("You can't go that way.\n")
            user_input(3, keys)
    elif current_room == 4:
        if movement == "d":
            print("You leave the cellar and enter the kitchen\n")
            user_input(2, keys)
        elif movement == "s" or "a" or "w":
            print("You can't go that way.\n")
        pickup = input(room_descriptions[4])
        if pickup == "y":
            print("You pick up the key and put it in your pocket.\n")
            user_input(4, keys+1)
        elif pickup == "n":
            print("You leave the key and continue looking around the cellar.\n")
            user_input(4, keys)
    elif current_room == 5:
        if movement == "a":
            print("You run back up the stairs to the ruins.\n")
            user_input(3, keys)
        else:
            print("You can't go that way.\n")
            user_input(5, keys)
    elif movement == "location":
        print("Current Room: " + rooms[current_room] +"\n")
        user_input(current_room, keys)

app()




